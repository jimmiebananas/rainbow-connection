package com.mowbs.rbc;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RainbowConnectionApp.class)
@WebAppConfiguration
public class RainbowConnectionAppTests {

	@Test
	public void contextLoads() throws UnknownHostException {
	    System.out.println(InetAddress.getLocalHost().getHostAddress());
	}

}
