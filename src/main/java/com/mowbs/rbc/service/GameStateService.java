package com.mowbs.rbc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.mowbs.rbc.domain.ColumnShiftRequest;
import com.mowbs.rbc.domain.GameState;
import com.mowbs.rbc.domain.JoinResponse;
import com.mowbs.rbc.domain.RowShiftRequest;
import com.mowbs.rbc.domain.SocketDestinations;

@Service
public class GameStateService {
    
    @Autowired
    private SimpMessagingTemplate msgTemplate;
    
    private GameState gameState = new GameState();
    
    public void resetGame() {
        synchronized (gameState) {
            gameState = new GameState();
        }
        
        sendUpdate();
    }
    
    public void startGame() {
        synchronized (gameState) {
            gameState.startGame();
        }
        
        sendUpdate();
    }
    
    public JoinResponse addPlayer() {
        Integer playerId = null;
        synchronized (gameState) {
            System.out.println("adding player!");
            playerId = gameState.addPlayer();
        }
        
        sendUpdate();
        
        return new JoinResponse(playerId, gameState.getGameId(), gameState.getColorMatrix());
    }
    
    public void shiftRow(RowShiftRequest request) {
        synchronized (gameState) {
            gameState.shiftRow(request);
        }
        
        sendUpdate();
    }
    
    public void shiftColumn(ColumnShiftRequest request) {
        synchronized (gameState) {
            gameState.shiftColumn(request);
        }
        
        sendUpdate();
    }
    
    public void passPlayerTurn(Integer playerId) {
        synchronized (gameState) {
            gameState.passPlayerTurn(playerId);
        }
        
        sendUpdate();
    }
    
    public GameState getGameState() {
        return gameState;
    }

    private void sendUpdate() {
        msgTemplate.convertAndSend(SocketDestinations.UPDATE_GAME_STATE, gameState);
    }
}
