package com.mowbs.rbc.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mowbs.rbc.domain.ColumnShiftRequest;
import com.mowbs.rbc.domain.GameState;
import com.mowbs.rbc.domain.JoinResponse;
import com.mowbs.rbc.domain.RowShiftRequest;
import com.mowbs.rbc.domain.SocketDestinations;
import com.mowbs.rbc.service.GameStateService;

@RestController
public class GameStateController {
	
	@Autowired
	private GameStateService gameStateService;
	
	@RequestMapping(value = "/gameState", method = RequestMethod.GET)
	@ResponseBody
	public GameState getGameState(HttpServletRequest request) throws Exception {		
		return gameStateService.getGameState();
	}
	
    @RequestMapping(value = "/joinGame", method = RequestMethod.GET)
    @ResponseBody
    public JoinResponse joinGame(HttpServletRequest request) throws Exception {
        return gameStateService.addPlayer();
    }
    
    @MessageMapping(SocketDestinations.START_GAME)
    public void startGame() {
        gameStateService.startGame();
    }    
	
	@MessageMapping(SocketDestinations.RESET_GAME)
	public void resetGame() {
	    gameStateService.resetGame();
	}
	
	@MessageMapping(SocketDestinations.ROW_SHIFTED)
	public void rowShifted(RowShiftRequest request) {
	    System.out.println(request);
	    gameStateService.shiftRow(request);
	}
	
	@MessageMapping(SocketDestinations.COLUMN_SHIFTED)
	public void columnShifted(ColumnShiftRequest request) {
	    System.out.println(request);
	    gameStateService.shiftColumn(request);
	}
	
    @MessageMapping(SocketDestinations.PASS_PLAYER_TURN)
    public void columnShifted(Integer playerId) {
        System.out.println("Passing player turn " + playerId);
        gameStateService.passPlayerTurn(playerId);
    }	

}
