package com.mowbs.rbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(value = {"com.mowbs.rbc.*"})
@SpringBootApplication
public class RainbowConnectionApp {

	public static void main(String[] args) {
		SpringApplication.run(RainbowConnectionApp.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
					.allowedOrigins("*")
					.allowedMethods("PUT", "DELETE", "GET", "POST", "HEAD", "OPTIONS")
					.allowedHeaders("Accept", "Content-Type", "Origin", "X-Requested-With", "X-Range", "Range")
					.exposedHeaders("Accept", "Content-Type", "Origin", "X-Requested-With", "X-Range", "Range")
					.allowCredentials(false).maxAge(3600);
			}

			@Override
			public void addViewControllers(ViewControllerRegistry registry) {
				registry.addViewController("/game").setViewName("forward:/game.html");
				registry.addViewController("/play").setViewName("forward:/play.html");
			}
		};
	}

}
