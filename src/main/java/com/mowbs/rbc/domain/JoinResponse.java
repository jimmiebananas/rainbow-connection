package com.mowbs.rbc.domain;

import java.util.List;
import java.util.Map;

public class JoinResponse {
    private Integer playerId;
    private String gameId;
    private Map<Integer, List<String>> colorMatrix;
    
    public JoinResponse(Integer playerId, String gameId, Map<Integer, List<String>> colorMatrix) {
        this.playerId = playerId;
        this.gameId = gameId;
        this.colorMatrix = colorMatrix;
    }
    
    public Integer getPlayerId() {
        return playerId;
    }
    
    public String getGameId() {
        return gameId;
    }
    
    public Map<Integer, List<String>> getColorMatrix() {
        return colorMatrix;
    }
}
