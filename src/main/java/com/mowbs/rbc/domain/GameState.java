package com.mowbs.rbc.domain;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class GameState {
    
    private static final Integer PLAYER_MOVE_COUNT = 3;

    private String gameId;
	private boolean started = false;
	private static String HOST;
	private Set<Integer> players = new HashSet<>();
	private Map<Integer, List<String>> colorMatrix = new HashMap<>();
	private Integer currentPlayerId;
	private Integer currentPlayerMovesRemaining = PLAYER_MOVE_COUNT;
	private Integer winningPlayer = null;
	
	private static final String[] COLORS = new String[] {"red","orange","yellow","green","blue","indigo","violet"};
	
	public GameState() {
	    colorMatrix.put(0, new ArrayList<>());
	    colorMatrix.put(1, new ArrayList<>());
	    colorMatrix.put(2, new ArrayList<>());
	    colorMatrix.put(3, new ArrayList<>());
	    gameId = UUID.randomUUID().toString();
	}
	
	static {
		try {
			HOST = InetAddress.getLocalHost().getHostAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getGameId() {
	    return gameId;
	}

	public String getHost() {
		return HOST;
	}

	public boolean isStarted() {
		return started;
	}
	
	public void startGame() {
	    this.started = true;
	    this.currentPlayerId = new Random().nextInt(this.players.size());
	    this.currentPlayerMovesRemaining = PLAYER_MOVE_COUNT;
	}
	
	public Integer getCurrentPlayerId() {
	    return currentPlayerId;
	}
	
	public Integer getCurrentPlayerMovesRemaining() {
	    return currentPlayerMovesRemaining;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}
	
	public Set<Integer> getPlayers() {
	    return players;
	}
	
	public Integer addPlayer() {
	    Integer newPlayerId = null;
	    synchronized (players) {
	        newPlayerId = players.size();
	        players.add(newPlayerId);
	        
	        for (int i = 0; i < 4; i++) {
	            String randomColor = COLORS[new Random().nextInt(COLORS.length)];
	            colorMatrix.get(i).add(newPlayerId, randomColor);
	        }
	    }
	    
	    return newPlayerId;
	}
	
	public void shiftRow(RowShiftRequest request) {
	    if (!started || currentPlayerId != request.getPlayerId()) {
	        return;
	    }
	    
	    synchronized (colorMatrix) {
	        String dir = request.getDirection();
	        Integer row = request.getRowId();
	        Integer player = request.getPlayerId();
	        
	        String color = colorMatrix.get(row).get(player);
	        System.out.println("you want to move " + color + " to the " + dir + "?");
	        
	        List<String> currentRow = colorMatrix.get(row);
	        System.out.println("Current row looks like this: " + currentRow);
	        List<String> newRow = new ArrayList<>(currentRow.size());
	        
	        for (int i = 0; i < currentRow.size(); i++) {
	            if ("left".equals(dir)) {
	                if (i == currentRow.size() - 1) {
	                    newRow.add(currentRow.get(0));
	                } else {
	                    newRow.add(currentRow.get(i + 1));
	                }
	            } else if ("right".equals(dir)) {
	                if (i == 0) {
	                    newRow.add(currentRow.get(currentRow.size() - 1));
	                } else {
	                    newRow.add(currentRow.get(i - 1));
	                }
	            }
	        }
	        
	        System.out.println("New row looks like this: " + newRow);
	        colorMatrix.put(row, newRow);
	    }
	    
	    if(!checkWinCondition()) {
	        decrementMoves();
	    }
	}
	
	public void shiftColumn(ColumnShiftRequest request) {
	       if (!started || currentPlayerId != request.getPlayerId()) {
	            return;
	        }
	    
	    System.out.println("Column shift request... " + request);
	    Integer playerId = request.getPlayerId();
	    String dir = request.getDirection();
	    String randomColor = COLORS[new Random().nextInt(COLORS.length)];
	    
	    synchronized (colorMatrix) {
	        if ("up".equals(dir)) {
	            for (int i = 0; i < 4; i++) {
                    if (i == 3) {
                        colorMatrix.get(i).set(playerId, randomColor);                        
                    } else {
                        colorMatrix.get(i).set(playerId, colorMatrix.get(i + 1).get(playerId));
                    }
	            }
	        } else if ("down".equals(dir)) {
	            for (int i = 3; i >= 0; i--) {
                    if (i == 0) {
                        colorMatrix.get(i).set(playerId, randomColor);
                    } else {
                        colorMatrix.get(i).set(playerId, colorMatrix.get(i - 1).get(playerId));
                    } 
	            }
	        }
	    }
	    
	    if (!checkWinCondition()) {
	        decrementMoves();
	    }
	}
	
	public void decrementMoves() {
	    currentPlayerMovesRemaining--;
	    
	    if (currentPlayerMovesRemaining == 0) {
	        currentPlayerMovesRemaining = 3;
	        if (currentPlayerId == players.size() - 1) {
	            currentPlayerId = 0;
	        } else {
	            currentPlayerId++;
	        }
	    }
	}
	
	public boolean checkWinCondition() {
	    boolean winnerExists = false;
	    
	    synchronized (colorMatrix) {
	        
	        for (int n = 0; n < players.size(); n++) {
               Set<String> uniquePlayerColors = new HashSet<>();
                for (int i = 0; i < 4; i++) {
                    uniquePlayerColors.add(colorMatrix.get(i).get(n));
                }
                
                if (uniquePlayerColors.size() == 1) {
                    winnerExists = true;
                    winningPlayer = n;
                    break;
                }
	        }
	    }
	    
	    return winnerExists;
	}
	
	public Integer getWinningPlayer() {
	    return winningPlayer;
	}
	
	public void passPlayerTurn(Integer playerId) {
	    if (!playerId.equals(currentPlayerId)) {
	        return;
	    }
	    
	    currentPlayerMovesRemaining = 1;
	    decrementMoves();
	}
	
	public Map<Integer, List<String>> getColorMatrix() {
	    return colorMatrix;
	}

}
