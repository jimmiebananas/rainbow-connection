package com.mowbs.rbc.domain;

public class ColumnShiftRequest implements ShiftRequest {
    private String direction;
    private Integer playerId;
    
    public String getDirection() {
        return direction;
    }
    
    public void setDirection(String direction) {
        this.direction = direction;
    }
    
    @Override
    public Integer getPlayerId() {
        return playerId;
    }
    
    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
    
    @Override
    public String toString() {
        return "column: " + playerId + " direction: " + direction;
    }    

}
