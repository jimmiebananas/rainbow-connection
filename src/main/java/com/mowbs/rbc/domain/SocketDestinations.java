package com.mowbs.rbc.domain;

public class SocketDestinations {
	public static final String GAME_UPDATES = "/gameSocket";
	public static final String UPDATE_GAME_STATE = "/update";
	public static final String ADD_PLAYER = "/addPlayer";
	public static final String RESET_GAME = "/resetGame";
	public static final String START_GAME = "/startGame";
	public static final String ROW_SHIFTED = "/rowShifted";
	public static final String COLUMN_SHIFTED = "/columnShifted";
	public static final String PASS_PLAYER_TURN = "/passPlayerTurn";
}
