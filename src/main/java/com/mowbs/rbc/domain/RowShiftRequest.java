package com.mowbs.rbc.domain;

public class RowShiftRequest implements ShiftRequest {
    private String direction;
    private Integer rowId;
    private Integer playerId;

    public String getDirection() {
        return direction;
    }
    public void setDirection(String direction) {
        this.direction = direction;
    }
    public Integer getRowId() {
        return rowId;
    }
    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }
    
    @Override
    public Integer getPlayerId() {
        return playerId;
    }
    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
    
    @Override
    public String toString() {
        return "row: " + rowId + " direction: " + direction + " player: " + playerId;
    }
}
