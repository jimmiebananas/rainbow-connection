getGameState();

function columnShifted(direction) {
	var playerId = getPlayerId();
	console.log('Player ' + playerId + ' shifted their column ' + direction);
	
	var shiftData = {
		direction: direction,
		playerId: playerId
	};
	
	socketClient.send('/app/columnShifted', {}, JSON.stringify(shiftData));
}

function rowShifted(direction, rowId) {	
	var playerId = getPlayerId();
	
	console.log(direction + " playerId: " + playerId + " rowId: " + rowId);
	
	var shiftData = {
		direction: direction,
		playerId: playerId,
		rowId: rowId
	};
	
	socketClient.send('/app/rowShifted', {}, JSON.stringify(shiftData));
}

function pass() {
	socketClient.send('/app/passPlayerTurn', {}, JSON.stringify(getPlayerId()));
}

function getGameState() {
	var http = new XMLHttpRequest();
	var url = restUrl + '/gameState';
	http.open("GET", url, true);

	http.setRequestHeader("Content-type", "application/json");

	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        var gameState = JSON.parse(http.responseText);
	        console.log('fetched game state ' + gameState);
	        var prevGameId = getGameId();
	        var prevPlayerId = getPlayerId();
	        var isActive = gameState.currentPlayerId === parseInt(prevPlayerId);
	        
	        if (gameState.gameId === prevGameId) {
	        	console.log('we are part of this game as playerId ' + prevPlayerId);
	        	getPlayerColors(gameState.colorMatrix, isActive);
	        } else {
	        	console.log('this must be a new game reset the stuff');
	        	saveGameId(null);
	        	savePlayerId(null);
	        }
	        
	    }
	}
	http.send();
}

function updatePlayer(gameState) {
	console.log('updatingPlayer: ', gameState);
	
	if (isValidPlayer()) {
		showPlaying();
	} else if (!gameState.started) {
		resetPlayer();
	}
	

}

function showPlaying() {
	var isActive = GAME_STATE.currentPlayerId === parseInt(getPlayerId());
	getPlayerColors(GAME_STATE.colorMatrix, isActive);
}

function showGameNotStarted() {
	
}

function showGameNotAvailable() {
	
}

function getPlayerColors(matrix, isActive) {
	var playerId = getPlayerId();
	console.log('getting colors for player ' + playerId);
	console.log('isActive: ', isActive);
	
	var colors = [];
	colors.push(matrix[0][playerId]);
	colors.push(matrix[1][playerId]);
	colors.push(matrix[2][playerId]);
	colors.push(matrix[3][playerId]);

	for (var i = 0; i < 4; i++) {
		document.getElementById(i).style.backgroundColor = colors[i];
	}
	
	document.getElementById('play').style.display = 'block';
	document.getElementById('join-wrapper').style.display = 'none';
	
	Array.prototype.forEach.call(document.getElementsByClassName('ctl'), function(el) {
	    el.style.opacity = isActive ? 1 : 0.1;
	});

}

function joinGame() {
	if (isValidPlayer()) {
		alert('you already joined');
		return;
	}
	
	var http = new XMLHttpRequest();
	var url = restUrl + '/joinGame';
	http.open("GET", url, true);

	http.setRequestHeader("Content-type", "application/json");

	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        var joinResponse = JSON.parse(http.responseText);
	    	playerId = joinResponse.playerId;
	    	gameId = joinResponse.gameId;
	        console.log('joined game as playerId ' + playerId + ' to gameId ' + gameId);
	        saveGameId(gameId);
	        savePlayerId(playerId);
	        document.getElementById('join-wrapper').style.display = 'none';
	        getPlayerColors(joinResponse.colorMatrix);
	    }
	}
	http.send();
}

function isValidPlayer() {
	var playerIdIsValid = parseInt(getPlayerId()) > -1;
	var gameIdIsValid = GAME_STATE && (getGameId() === GAME_STATE.gameId);
	
	return playerIdIsValid && gameIdIsValid;
}

function getPlayerId() {
	return sessionStorage.getItem('rbcPlayerId');
}

function savePlayerId(playerId) {
	sessionStorage.setItem('rbcPlayerId', playerId);
}

function getGameId() {
	return sessionStorage.getItem('rbcGameId');
}

function saveGameId(gameId) {
	sessionStorage.setItem('rbcGameId', gameId);
}

function resetPlayer() {
	document.getElementById('play').style.display = 'none';
	document.getElementById('join-wrapper').style.display = 'block';
}