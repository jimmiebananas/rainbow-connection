var socketUrl = 'ws://' + window.location.hostname + ':8080/gameSocket';
var restUrl = 'http://' + window.location.hostname + ':8080';
var playerId = null;
var GAME_STATE = null;

var socket = new WebSocket(socketUrl);
socketClient = new Stomp.over(socket);
socketClient.debug = null;

socketClient.connect({}, function (connectedFrame) {
    console.log('socket opened...');
    socketClient.subscribe('/update', function (frame) {
    	GAME_STATE = JSON.parse(frame.body);
    	doUpdates(GAME_STATE);
    });
});

function doUpdates(gameState) {
	if (document.getElementById('grid')) {
		updateGame(gameState);
	} else if (document.getElementById('play')) {
		updatePlayer(gameState);
	}
}
