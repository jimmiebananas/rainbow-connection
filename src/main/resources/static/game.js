getGameStateForGame();

function updateGame(gameState) {
	document.getElementById('hostname').innerHTML = 'http://' + gameState.host + ':8080/play';
	console.log('updatingGame: ', gameState);
	var winningPlayer = parseInt(gameState.winningPlayer);
	
	if (!(winningPlayer > -1)) {
		document.getElementById('winner-modal').style.display = 'none';
	}
		
	if (gameState.started === true) {
		showRunningGame(gameState, winningPlayer);
	} else if (!gameState.started && gameState.players.length > 0) {
		console.log('Still waiting for game to start');
		var msg = 'There are ' + gameState.players.length + ' players. Waiting for game to start';
		document.getElementById('grid').innerHTML = msg;
		document.getElementById('start-game-btn').style.display = 'block';
	} else {
		console.log('Still waiting for players to join');
		document.getElementById('grid').innerHTML = 'Waiting for players';
		document.getElementById('start-game-btn').style.display = 'none';
	} 
}

function showWinner(winningPlayer) {
	var winnerModal = document.getElementById('winner-modal');
	winnerModal.innerHTML = 'Player ' + winningPlayer + ' wins!!'; 
	winnerModal.style.display = 'block';
}

function showRunningGame(gameState, winningPlayer) {
	document.getElementById('start-game-btn').style.display = 'none';
	
	var grid = document.getElementById('grid');
	var newGrid = document.createElement('div');
	
	var selectedPlayerId = gameState.currentPlayerId;
	var remaining = gameState.currentPlayerMovesRemaining;
	var header = buildPlayerHeaderRowHtml(gameState.players, selectedPlayerId, remaining);
	newGrid.append(header);
	
	Object.entries(gameState.colorMatrix).forEach(function(entry, idx) {
		var colors = entry[1];
		var newRow = buildRowHtml(colors, selectedPlayerId);
		newGrid.append(newRow);
	});
	
	grid.innerHTML = newGrid.innerHTML;
	
	if (winningPlayer > -1) {
		showWinner(winningPlayer);
	}
}

function showPlayersButNotStartedGame() {
	
}

function showNoPlayersNotStartedGame() {
	
}

function buildPlayerHeaderRowHtml(players, selectedPlayerId, remaining) {
	var header = document.createElement('div');
	header.className = 'header-row';
	var numberOfPlayers = players.length;
	var percentWidth = (100 / numberOfPlayers) - 2;
	players.forEach(function(player, idx) {
		var newPlayer = document.createElement('div');
		newPlayer.className = idx === selectedPlayerId ? 'header-row-player-selected' : 'header-row-player';
		newPlayer.style.width = percentWidth + '%';
		newPlayer.innerHTML = idx === selectedPlayerId ? 'Player ' + idx + ' (' + remaining + ' moves)' : 'Player ' + idx;
		header.append(newPlayer);
	});
	
	return header;
}

function buildRowHtml(colors, selectedPlayerId) {
	var newRow = document.createElement('div');
	newRow.className = 'row';
	var numberOfColors = colors.length;
	var percentWidth = (100 / numberOfColors) - 2; 
	
	colors.forEach(function(color, idx) {
		var newColor = document.createElement('div');
		newColor.className = 'grid-color';
		newColor.style.backgroundColor = color;
		newColor.style.width = percentWidth + '%';
		newRow.append(newColor);
	});
	
	return newRow;
}

function getGameStateForGame() {
	var http = new XMLHttpRequest();
	var url = restUrl + '/gameState';
	http.open("GET", url, true);

	http.setRequestHeader("Content-type", "application/json");

	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        var gameState = JSON.parse(http.responseText);
	        console.log('fetched game state ' + gameState);
	        updateGame(gameState);
	    }
	}
	http.send();
}

function resetGame() {
	socketClient.send('/app/resetGame', {}, '');
}

function startGame() {
	console.log('starting game');
	socketClient.send('/app/startGame', {}, '');
}